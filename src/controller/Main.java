package controller;

public class Main 
{
	public static final String VERSION = "0.3.2";
	public static final String LICENSE =
			"Copyright (C) 2016 Carlos F. M. Faruolo (aka Hydren)\nE-mail: <5carlosfelipe5@gmail.com>\n\n" +
			"This program is free software: you can redistribute it and/or modify\n" +
			"it under the terms of the GNU General Public License as published by\n" +
			"the Free Software Foundation, either version 3 of the License, or\n" +
			"(at your option) any later version.\n\nThis program is distributed in the hope that it will be useful,\n" +
			"but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
			"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
			"GNU General Public License for more details.\n\n" +
			"You should have received a copy of the GNU General Public License\n" +
			"along with this program.  If not, see <http://www.gnu.org/licenses/>.";
	
	public static void main(String[] args) 
	{
		if(args.length > 0 && (args[0].equals("-v") || args[0].equals("--version")))
			System.out.println(VERSION);
		else
			new ApplicationController().start();
	}
}
