package controller;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Matrix<T> implements Iterable<T>
{
	private ArrayList<ArrayList<T>> list = new ArrayList<ArrayList<T>>();
	
	public Matrix() 
	{
		appendRow();
		appendColumn();
	}
	
	public Matrix(int columns, int rows)
	{
		this();
		appendRows(rows-1);
		appendColumns(columns-1);
	}
	
	public T get(int i, int j)
	{
		return list.get(i).get(j);
	}
	
	public void set(int i, int j, T t)
	{
		list.get(i).set(j, t);
	}
	
	public void appendRow()
	{
		appendRow((T) null);
	}
	
	public void appendRow(T defaultValue)
	{
		//add new row
		list.add(new ArrayList<T>()); 

		//enlarge row to be the same size as the previous
		if(list.size() > 1) for(int i = 0; i < list.get(list.size()-2).size(); i++) 
			list.get(list.size()-1).add(defaultValue);
	}
	
	public void appendRow(Class<T> cls) throws InstantiationException, IllegalAccessException
	{
		//add new row
		list.add(new ArrayList<T>()); 

		//enlarge row to be the same size as the previous
		if(list.size() > 1) for(int i = 0; i < list.get(list.size()-2).size(); i++) 
			list.get(list.size()-1).add(cls.newInstance());
	}
	
	public void appendColumn()
	{
		appendColumn((T) null);
	}
	
	public void appendColumn(T defaultValue)
	{
		//add a new null element to each inner list.
		for(ArrayList<T> l : list)
			l.add(defaultValue);
	}
	
	public void appendColumn(Class<T> cls) throws InstantiationException, IllegalAccessException
	{
		//add a new null element to each inner list.
		for(ArrayList<T> l : list)
			l.add(cls.newInstance());
	}
	
	public void appendRows(int amount)
	{
		for(int i = 0; i < amount; i++)
			appendRow();
	}
	
	public void appendColumns(int amount)
	{
		for(int i = 0; i < amount; i++)
			appendColumn();
	}
	
	public void addRow(int index)
	{
		addRow(index, null);
	}
	
	public void addRow(int index, T defaultValue)
	{
		list.add(index, new ArrayList<T>());
		
		//enlarge row to be the same size as the previous
		if(index > 0) for(int i = 0; i < list.get(index-1).size(); i++) 
			list.get(index).add(defaultValue);
		
		//if index == 0 (first) enlarge row to be the same size as the next, if there are more than one.
		if(index == 0 && list.size() > 1)
			for(int i = 0; i < list.get(1).size(); i++) 
				list.get(0).add(defaultValue);
	}
	
	public void addColumn(int index)
	{
		addColumn(index, null);
	}
	
	public void addColumn(int index, T defaultValue)
	{
		//add a new null element to each inner list.
		for(ArrayList<T> l : list)
			l.add(index, defaultValue);
	}
	
	public void clearRow(int index)
	{
		for(int i = 0; i < list.get(index).size(); i++)
			list.get(index).set(i, null);
	}
	
	public void clearColumn(int index)
	{
		//add a new null element to each inner list.
		for(ArrayList<T> l : list)
			l.set(index, null);
	}
	
	public void removeRow(int index)
	{
		list.remove(index);
	}
	
	public void removeColumn(int index)
	{
		for(ArrayList<T> l : list)
			l.remove(index);
	}
	
	public void removeLastRow()
	{
		removeRow(list.size()-1);
	}
	
	public void removeLastColumn()
	{
		removeColumn(list.get(0).size()-1);
	}
	
	public int getRowCount()
	{
		return list.size();
	}
	
	public int getColumnsCount()
	{
		return list.get(0).size();
	}
	
	public boolean contains(T t)
	{
		for(ArrayList<T> l : list)
			if(l.contains(t))
				return true;
		
		return false;
	}
	
	public int[] indexesOf(T t)
	{
		for(ArrayList<T> l : list) for(T mt : l) if(t.equals(mt)) return new int[] { list.indexOf(l), l.indexOf(mt) };
		return null;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
		{
			int i=0, j=0;
			
			@Override
			public boolean hasNext() 
			{
				if(i < list.size() && j < list.get(i).size())
					return true;
				else return false;
			}

			@Override
			public T next() 
			{
				if(i > list.size() - 1 || j > list.get(i).size() - 1)
					throw new NoSuchElementException();
				
				T next = get(i, j);
				
				if(j < list.get(i).size() - 1)
					j++;
				else
				{
					j = 0;
					i++;
				}
				
				return next;
			}

			@Override
			public void remove() 
			{
				throw new InvalidParameterException("Operation not supported! Remove();");
			}
		};
	}
}
