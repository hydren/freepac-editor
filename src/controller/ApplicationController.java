package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Properties;

import javax.swing.JOptionPane;

import model.CommonSettings;
import model.StageFile;
import view.GridSlot;
import view.MainWindow;

public class ApplicationController 
{
	private MainWindow mainWindow;
	
	public StageFile currentStage;
	
	public CommonSettings commons = new CommonSettings();
	
	public String path;
	
	public ApplicationController()
	{
		path = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile().getAbsolutePath();
		try { commons.load(); }
		catch (FileNotFoundException e) {}
		catch (Exception e) { e.printStackTrace(); }
		currentStage = new StageFile(null);
		mainWindow = new MainWindow(this);
	}
	
	public void start()
	{
		mainWindow.show();
	}
	
	public void shutdown()
	{
		try 
		{
			commons.store();
		} 
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Fatal error!", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		System.exit(0);
	}
	
	public void saveStageFile(char[][] grid, Properties defs, String filename) throws FileNotFoundException, IOException
	{
		currentStage.grid = grid;
		currentStage.fileIdentifier = filename.replace(".def", "");
		currentStage.definitions = defs;
		currentStage.save();
	}
	
	public void loadStageFile(String filename) throws InputMismatchException, FileNotFoundException, NullPointerException, IOException
	{
		currentStage.fileIdentifier = filename.replace(".def", "");
		currentStage.load();
	}
	
	public char gridSlotValueToChar(GridSlot slot)
	{
		if(slot.value.equalsIgnoreCase("pellet"))
			return '.';
		
		if(slot.value.equalsIgnoreCase("pellet and slow zone"))
			return ',';
		
		if(slot.value.equalsIgnoreCase("energizer"))
			return '*';
		
		if(slot.value.equalsIgnoreCase("nothing"))
			return ' ';
		
		if(slot.value.equalsIgnoreCase("wall"))
			return '#';
		
		if(slot.value.equalsIgnoreCase("fence"))
			return '=';
		
		if(slot.value.equalsIgnoreCase("outside"))
			return '/';
		
		if(slot.value.equalsIgnoreCase("slow zone"))
			return '´';
		
		if(slot.value.equalsIgnoreCase("player start"))
			return '$';
		
		if(slot.value.equalsIgnoreCase("enemy start"))
			return '@';
		
		if(slot.value.equalsIgnoreCase("v warp"))
			return '¡';
		
		if(slot.value.equalsIgnoreCase("h warp"))
			return '!';
		
		return ' ';
	}
	
	public String charToGridSlotValue(char c)
	{
		if(c == '.')
			return "pellet";

		if(c == ',')
			return "pellet and slow zone";

		if(c == '*')
			return "energizer";

		if(c == ' ')
			return "nothing";

		if(c == '#')
			return "wall";

		if(c == '=')
			return "fence";

		if(c == '/')
			return "outside";

		if(c == '´')
			return "slow zone";

		if(c == '$')
			return "player start";

		if(c == '@')
			return "enemy start";

		if(c == '¡')
			return "v warp";

		if(c == '!')
			return "h warp";
		
		return "nothing";
	}
}
