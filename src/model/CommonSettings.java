package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

public class CommonSettings 
{
	public String lastLoadPath, lastSavePath;
	public Integer gridSlotSize = 32;
	public Integer mainSplitPosition = 192, leftSplitPanePosition = 256;
	public Integer windowWidth = 800, windowHeight = 600;
	public Boolean maximized = false;
	
	private File file = new File("common.properties");
	
	public void store() throws IllegalArgumentException, IllegalAccessException, IOException
	{
		Properties p = new Properties();
		for(Field f : CommonSettings.class.getFields())
			if(f.get(this) != null) p.setProperty(f.getName(), f.get(this).toString());
		
		if(!file.exists())
		{
			file.createNewFile();
		}
		
		FileOutputStream fos = new FileOutputStream(file);
		p.store(fos, "You can safely delete this file, but its a pain to.");
		fos.close();
	}
	
	public void load() throws IOException, IllegalArgumentException, IllegalAccessException
	{
		Properties p = new Properties();
		FileInputStream fis = new FileInputStream(file);
		p.load(fis);
		fis.close();
		
		for(Field f : CommonSettings.class.getFields())
			if(p.containsKey(f.getName()))
			{
				if(f.getType().equals(String.class))
					f.set(this, p.getProperty(f.getName()));
				if(f.getType().equals(Boolean.class))
					f.set(this, Boolean.parseBoolean(p.getProperty(f.getName())));
				if(f.getType().equals(Integer.class)) try {
					f.set(this, Integer.parseInt(p.getProperty(f.getName())));
				}catch (NumberFormatException e) { e.printStackTrace(); }
			}
	}
}
