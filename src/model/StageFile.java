package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;

import controller.Main;

public class StageFile 
{
	public String fileIdentifier;
	
	public Properties definitions;
	public char[][] grid;
	
	public StageFile(String ref)
	{
		fileIdentifier = ref;
		definitions = new Properties();
		grid = null;
	}
	
	static void copyValuesFromIfAbsent(Properties prop, String[][] keyPairs)
	{
	    	for(int i = 0; i < keyPairs.length; i++)
	    		if(!prop.containsKey(keyPairs[i][0]) && prop.containsKey(keyPairs[i][1])) 
				prop.put(keyPairs[i][0], prop.get(keyPairs[i][1]));
	}
	
	static void replaceValuesOfKey(Properties prop, String key, String[][] valuePairs)
	{
		if(prop.containsKey(key))
		    for(int i = 0; i < valuePairs.length; i++)
			if(prop.get(key).equals(valuePairs[i][1]))
			    prop.put(key, valuePairs[i][0]);
	}
	
	public void load() throws IOException, FileNotFoundException, InputMismatchException, NullPointerException
	{
		File defFile = new File(fileIdentifier + ".def"), mapFile = new File(fileIdentifier + ".map");
		
		FileInputStream fisProp = new FileInputStream(defFile);
		definitions.clear();
		definitions.load(fisProp);
		fisProp.close();
		
		// support for portuguese fields
		copyValuesFromIfAbsent(definitions, new String[][]{
			{"width", "largura"},
			{"height", "altura"},
			{"name", "nome"},
			{"color", "cor"},
			{"background", "fundo"},
			{"style", "estilo"},
			{"music", "musica"},
			{"enemies", "fantasmas"}
		});
		replaceValuesOfKey(definitions, "style", new String[][]{
		    {"rounded", "arredondado"},
		    {"sloped", "inclinado"},
		    {"squared", "quadrado"}
		});
		
		int lines = Integer.parseInt(definitions.getProperty("height").trim());
		int columns = Integer.parseInt(definitions.getProperty("width").trim());
		
		//read stage map (chars)
		Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream(mapFile), "UTF-8"));
		ArrayList<Character> chars = new ArrayList<Character>();
		while(scanner.hasNextLine()) for(char c : scanner.nextLine().toCharArray())
		{
			chars.add(c);
		}
		scanner.close(); //chars were read
		
		grid = new char[lines][columns];
		if(chars.size() != lines*columns) throw new InputMismatchException("Ill formatted map: expected "+(lines*columns)+" slots but "+chars.size()+" were found!");
		
		//parse chars to char grid
		for(int i = 0, k = 0; i < lines; i++) for(int j = 0; j < columns; j++)
		{
			grid[i][j] = chars.get(k++);
		}
	}
	
	public void save() throws IOException, FileNotFoundException
	{
		File defFile = new File(fileIdentifier + ".def"), mapFile = new File(fileIdentifier + ".map");
		if( ! defFile.exists()) defFile.createNewFile();
		if( ! mapFile.exists()) mapFile.createNewFile();
		
		FileOutputStream fosProp = new FileOutputStream(defFile);
		definitions.store(fosProp, "Edited with freepac-editor v"+Main.VERSION);
		fosProp.close();
		
		PrintWriter printer = new PrintWriter(mapFile, "UTF-8");
		
		//parse chars to char grid
		for(int i = 0; i < grid.length; i++) 
		{
			for(int j = 0; j < grid[i].length; j++)
			{
				printer.write(grid[i][j]);
			}
			printer.write('\n');
		}
		printer.close();
	}
	
}
