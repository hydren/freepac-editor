package model;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.Random;

public abstract class ColorExtra extends Color
{
	private static final long serialVersionUID = 1L; public ColorExtra(int value) { super(value); /* Auto-generated constructor stub */ }

	public static final Color
	
	maroon = new Color(128, 0, 0), darkRed = maroon,
	navy = new Color(0, 0, 128), darkBlue = navy,
	darkGreen = new Color(0, 128, 0), lime = Color.green,
	purple = new Color(128, 0, 128), //html/css, patriarch purple
	olive = new Color(128, 128, 0),
	teal = new Color(0, 128, 128),
	
	violet = new Color(127, 0, 255),  //Color wheel violet

	royalPurple = new Color(120, 81, 169),
	electricPurple = new Color(143, 0, 255),
	darkGrayPurple = new Color(91, 82, 91),
	
	rosePink =  new Color(255, 102, 204),
	hotPink = new Color(255, 105, 180),
	lightHotPink = new Color(255, 173, 223),
	
	pumpkin = new Color(255, 117, 24),
	darkPaleYellow = new Color(139, 139, 122),
	
	paleGreen = new Color(152, 251, 152),
	darkPaleGreen = new Color(90,100,90),
	aquamarine = new Color(127, 255, 212),
	
	
	glassSmoke = new Color(0, 0, 0, 128); //used to create a darkened filter
	
	// Parse a color from a string
	public static Color toColor(String str)
	{
		//if empty or null, abort
		if(str == null || str.trim().length() == 0)
			return null;
		
		//try to parse color by name
		try {
			//slick2d colors
			for(Field f : Color.class.getFields()) if(f.getType() == Color.class)
			{
				if(f.getName().trim().equalsIgnoreCase(str))
					return (Color) f.get(null);
			}
			//color-extra colors
			for(Field f : ColorExtra.class.getFields()) if(f.getType() == Color.class)
			{
				if(f.getName().trim().equalsIgnoreCase(str))
					return (Color) f.get(null);
			}
			//TODO java.awt colors
		} 
		catch (IllegalArgumentException e) 	{ e.printStackTrace(); return null; } 
		catch (IllegalAccessException e) 	{ e.printStackTrace(); return null; }
		
		//try to parse color by r,g,b notation
		if(str.split(",").length == 3)
		{
			String[] tokens = str.split(",");
			try
			{
				return new Color(Integer.parseInt(tokens[0].trim()), Integer.parseInt(tokens[1].trim()), Integer.parseInt(tokens[2].trim()));
			}
			catch(NumberFormatException e)
			{
				return null;
			}
		}
		
		//try to parse color by #XXXXXX notation or hexa
		if(str.startsWith("#"))
			str = str.substring(1);

		try
		{
			return new Color(Integer.parseInt(str));
		}
		catch(NumberFormatException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean isColor(String str)
	{
		return toColor(str)!=null;
	}
	
	public static Color toColor(String str, Color caseFailureColor)
	{
		return isColor(str)? toColor(str) : caseFailureColor;
	}

	public static Color getContrastColor(Color color) 
		{
			double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
			return y >= 128 ? Color.black : Color.white;
	//		return new Color(255-color.getRed(),
	//               255-color.getGreen(),
	//               255-color.getBlue());
		}

	private static Random random = new Random();
	public static Color getRandomColor()
	{
		switch (random.nextInt(12)) {
		case 0: return Color.black;
		case 1: return Color.blue;
		case 2: return Color.cyan;
		case 3: return Color.yellow;
		case 4: return Color.gray;
		case 5: return Color.green;
		case 6: return Color.lightGray;
		case 7: return Color.magenta;
		case 8: return Color.orange;
		case 9: return Color.pink;
		case 10: return Color.red;
		case 11: return Color.white;
		default: return Color.darkGray;
		}
	}
}
