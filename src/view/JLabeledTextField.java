package view;

import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JLabeledTextField extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5624066470941881219L;
	public JLabel label;
	public JTextField field;
	
	public JLabeledTextField() {
		super();
		field = new JTextField();
		label = new JLabel();
		add(label); add(field);
	}
	
	public JLabeledTextField(String txtLabel)
	{
		super();
		field = new JTextField();
		label = new JLabel(txtLabel);
		add(label); add(field);
	}
	
	public JLabeledTextField(String txtLabel, String txtValue)
	{
		super();
		field = new JTextField(txtValue);
		label = new JLabel(txtLabel);
		add(label); add(field);
	}
	
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
	}
}
