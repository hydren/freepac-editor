package view;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import model.ColorExtra;

public class GridSlot extends JPanel
{
	private static final long serialVersionUID = -9189249148279694437L;
	public String value;
	
	public GridSlot () 
	{
		super();
		Dimension tileDimension = new Dimension(MainWindow.controller.commons.gridSlotSize, MainWindow.controller.commons.gridSlotSize);
		setMaximumSize(tileDimension);
		setMinimumSize(tileDimension);
		setPreferredSize(tileDimension);
		setBorder(new LineBorder(Color.DARK_GRAY));
		mark("nothing");
	}
	
	public GridSlot (String value) 
	{ 
		this();
		this.mark(value);
	}
	
	public void mark(String value)
	{
		this.removeAll();
		this.value = value;
		if(value.equalsIgnoreCase("pellet") || value.equalsIgnoreCase("pellet and slow zone"))
		{
			JLabel label = new JLabel(".");
			label.setForeground(Color.white);
			setToolTipText("Pellet");
			add(label);
		}
		
		if(value.equalsIgnoreCase("energizer"))
		{
			JLabel label = new JLabel("*");
			label.setForeground(Color.yellow);
			setToolTipText("Energizer");
			add(label);
		}
		
		if(value.equalsIgnoreCase("nothing") || value.equalsIgnoreCase("pellet") || value.equalsIgnoreCase("energizer"))
			setBackground(Color.black);
		
		if(value.equalsIgnoreCase("wall"))
		{
			setBackground(ColorExtra.navy);
			setToolTipText("Wall");
		}
		
		if(value.equalsIgnoreCase("fence"))
		{
			setBackground(Color.gray);
			setToolTipText("Fence");
		}
		
		if(value.equalsIgnoreCase("outside"))
		{
			setBackground(ColorExtra.darkPaleGreen);
			setToolTipText("Outside map");
		}
		
		if(value.equalsIgnoreCase("slow zone") || value.equalsIgnoreCase("pellet and slow zone"))
		{
			setBackground(ColorExtra.darkGrayPurple);
			setToolTipText((getToolTipText()!=null?getToolTipText()+" and ":"") + "Slow zone (enemies)");
		}
		
		if(value.equalsIgnoreCase("player start"))
		{
			setBackground(Color.green);
			setToolTipText("Player start");
		}
		
		if(value.equalsIgnoreCase("enemy start"))
		{
			setBackground(Color.red);
			setToolTipText("Enemy start");
		}
		
		if(value.equalsIgnoreCase("v warp"))
		{
			setBackground(ColorExtra.aquamarine);
			setToolTipText("Warp (vertical)");				
		}
		
		if(value.equalsIgnoreCase("h warp"))
		{
			setBackground(Color.cyan);
			setToolTipText("Warp (horizontal)");
		}
	}
	
}
