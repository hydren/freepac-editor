package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import controller.ApplicationController;
import controller.Main;
import controller.Matrix;

public class MainWindow 
{
	private JFrame frame;
	private JMenuItem mntmNewStage;
	private JPanel panelGrid;
	private JMenuItem mntmExit;
	private JMenuItem mntmAbout;
	private Matrix<GridSlot> gridPanels;
	
	private boolean modified = false;
	static ApplicationController controller;
	private EventListener listener;
	private JSplitPane splitPaneMain;
	private JSplitPane splitPaneLeft;
	private JScrollPane scrollPaneTintsButtons;
	private JPanel panelTints;
	private ButtonGroup btngrpTint;
	private JRadioButton rdbtnClear;
	private JRadioButton rdbtnWall;
	private JRadioButton rdbtnFence;
	private JRadioButton rdbtnOutside;
	private JRadioButton rdbtnHorizWarp;
	private JRadioButton rdbtnPlayerStart;
	private JRadioButton rdbtnEnemyStart;
	private JRadioButton rdbtnPellet;
	private JRadioButton rdbtnEnergizer;
	private JRadioButton rdbtnSlowZone;
	private JRadioButton rdbtnPelletAndSlow;
	private JRadioButton rdbtnVerticWarp;
	private JPanel panelTintButtons;
	private JLabel lblTintTitle;
	private JPanel panelDefs;
	private JLabel lblMapDefinitions;
	private JScrollPane scrollPaneDefs;
	private JPanel panelDefFields;
	private JLabeledTextField labeledTextFieldName;
	private JLabeledTextField labeledTextFieldColor;
	private JLabeledTextField labeledTextFieldEnemies;
	private JLabel lblOptionals;
	private JLabeledTextField labeledTextFieldStyle;
	private JLabeledTextField labeledTextFieldBg;
	private JLabeledTextField labeledTextFieldSong;
	private JPanel panelNoIntroTune;
	private JCheckBox chckbxNoIntroTune;
	private JMenuItem mntmLoadStage;
	private JMenuItem mntmSaveStage;
	private JToolBar toolBar;
	private JLabel lblMapSize;
	private JSpinner spinnerWidth;
	private JLabel lblX;
	private JSpinner spinnerHeight;
	private JLabel lblGridSize;
	private JSpinner spinnerGridSlotSize;
	private Component horizontalStrut;
	private JButton btnReset;
	private Component horizontalStrut_1;
	private JToggleButton tglbtnBrush;
	private JToggleButton tglbtnBucket;
	private Component horizontalStrut_2;
	
	public MainWindow(ApplicationController controller)
	{
		MainWindow.controller = controller;
		this.listener = new EventListener();
		this.frame = new JFrame("freepac-editor");
		this.frame.addWindowListener(listener);
		
		JMenuBar menuBar = new JMenuBar();
		this.frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmNewStage = new JMenuItem("New stage");
		mntmNewStage.addActionListener(listener);
		mnFile.add(mntmNewStage);
		
		mntmLoadStage = new JMenuItem("Load stage");
		mntmLoadStage.addActionListener(listener);
		mnFile.add(mntmLoadStage);
		
		mntmSaveStage = new JMenuItem("Save stage");
		mntmSaveStage.addActionListener(listener);
		mnFile.add(mntmSaveStage);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(listener);
		mnFile.add(mntmExit);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(listener);
		mnHelp.add(mntmAbout);
		
		gridPanels = new Matrix<GridSlot>();
		
		JScrollPane scrollPaneGrid = new JScrollPane();

		panelGrid = new JPanel();
		panelGrid.setBorder(null);
		panelGrid.setLayout(new GridLayout(1, 0, 0, 0));
		JPanel wrapperPanelGrid = new JPanel();
		wrapperPanelGrid.setLayout(new BoxLayout(wrapperPanelGrid, BoxLayout.Y_AXIS));
		wrapperPanelGrid.add(panelGrid);
		scrollPaneGrid.setViewportView(wrapperPanelGrid);
		
		splitPaneMain = new JSplitPane();
		splitPaneMain.setRightComponent(scrollPaneGrid);
		frame.getContentPane().add(splitPaneMain, BorderLayout.CENTER);
		
		splitPaneLeft = new JSplitPane();
		splitPaneLeft.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPaneMain.setLeftComponent(splitPaneLeft);
		
		panelTints = new JPanel();
		panelTints.setLayout(new BorderLayout(0, 0));
		splitPaneLeft.setLeftComponent(panelTints);
		
		scrollPaneTintsButtons = new JScrollPane();
		panelTints.add(scrollPaneTintsButtons);
		
		panelTintButtons = new JPanel();
		panelTintButtons.setLayout(new BoxLayout(panelTintButtons, BoxLayout.Y_AXIS));
		btngrpTint = new ButtonGroup();
		
		rdbtnClear = new JRadioButton("Clear");
		rdbtnClear.setSelected(true);
		btngrpTint.add(rdbtnClear);
		panelTintButtons.add(rdbtnClear);
		
		rdbtnWall = new JRadioButton("Wall");
		btngrpTint.add(rdbtnWall);
		panelTintButtons.add(rdbtnWall);
		
		rdbtnFence = new JRadioButton("Fence");
		btngrpTint.add(rdbtnFence);
		panelTintButtons.add(rdbtnFence);
		
		rdbtnOutside = new JRadioButton("Outside");
		btngrpTint.add(rdbtnOutside);
		panelTintButtons.add(rdbtnOutside);
		
		rdbtnHorizWarp = new JRadioButton("Warp (horizontal)");
		btngrpTint.add(rdbtnHorizWarp);
		panelTintButtons.add(rdbtnHorizWarp);
		
		rdbtnVerticWarp = new JRadioButton("Warp (vertical)");
		btngrpTint.add(rdbtnVerticWarp);
		panelTintButtons.add(rdbtnVerticWarp);
		
		rdbtnSlowZone = new JRadioButton("Slow zone (enemies)");
		btngrpTint.add(rdbtnSlowZone);
		panelTintButtons.add(rdbtnSlowZone);
		
		rdbtnPlayerStart = new JRadioButton("Player start");
		btngrpTint.add(rdbtnPlayerStart);
		panelTintButtons.add(rdbtnPlayerStart);
		
		rdbtnEnemyStart = new JRadioButton("Enemy start");
		btngrpTint.add(rdbtnEnemyStart);
		panelTintButtons.add(rdbtnEnemyStart);
		
		rdbtnPellet = new JRadioButton("Pellet");
		btngrpTint.add(rdbtnPellet);
		panelTintButtons.add(rdbtnPellet);
		
		rdbtnPelletAndSlow = new JRadioButton("Pellet and slow zone (enemies)");
		btngrpTint.add(rdbtnPelletAndSlow);
		panelTintButtons.add(rdbtnPelletAndSlow);
		
		rdbtnEnergizer = new JRadioButton("Energizer");
		btngrpTint.add(rdbtnEnergizer);
		panelTintButtons.add(rdbtnEnergizer);
		
		scrollPaneTintsButtons.setViewportView(panelTintButtons);
		
		lblTintTitle = new JLabel("Map edit");
		lblTintTitle.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblTintTitle.setFont(new Font("Dialog", Font.BOLD, 12));
		panelTints.add(lblTintTitle, BorderLayout.NORTH);
		
		panelDefs = new JPanel();
		splitPaneLeft.setRightComponent(panelDefs);
		panelDefs.setLayout(new BorderLayout(0, 0));
		
		lblMapDefinitions = new JLabel("Map definitions");
		lblMapDefinitions.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		lblMapDefinitions.setFont(new Font("Dialog", Font.BOLD, 12));
		panelDefs.add(lblMapDefinitions, BorderLayout.NORTH);
		
		scrollPaneDefs = new JScrollPane();
		panelDefs.add(scrollPaneDefs, BorderLayout.CENTER);
		
		panelDefFields = new JPanel();
		panelDefFields.setMaximumSize(new Dimension(32767, 32));
		scrollPaneDefs.setViewportView(panelDefFields);
		panelDefFields.setLayout(new BoxLayout(panelDefFields, BoxLayout.Y_AXIS));
		
		labeledTextFieldName = new JLabeledTextField("Name:");
		labeledTextFieldName.setMaximumSize(new Dimension(2147483647, 32));
		panelDefFields.add(labeledTextFieldName);
		labeledTextFieldName.field.setColumns(10);
		
		labeledTextFieldColor = new JLabeledTextField("Color:");
		labeledTextFieldColor.field.setToolTipText("The color of the walls. Use RGB notation (i.e. 255,128,32), hexadecimal or literal (limited)");
		labeledTextFieldColor.field.setColumns(10);
		labeledTextFieldColor.setMaximumSize(new Dimension(2147483647, 32));
		panelDefFields.add(labeledTextFieldColor);
		
		labeledTextFieldEnemies = new JLabeledTextField("Enemies:");
		labeledTextFieldEnemies.field.setToolTipText("The enemies to be spawned on the stage. Use 'default', 'classic', 'dummy' or a comma-separated list of enemy types (i.e. 'default_0, default_3, default_2, default_0')");
		labeledTextFieldEnemies.field.setColumns(10);
		labeledTextFieldEnemies.setMaximumSize(new Dimension(2147483647, 32));
		panelDefFields.add(labeledTextFieldEnemies);
		
		lblOptionals = new JLabel("Optionals");
		lblOptionals.setFont(new Font("Dialog", Font.BOLD, 12));
		lblOptionals.setForeground(SystemColor.textInactiveText);
		lblOptionals.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelDefFields.add(lblOptionals);
		
		labeledTextFieldStyle = new JLabeledTextField("Style:");
		labeledTextFieldStyle.field.setToolTipText("The wall's style. Use 'rounded', 'squared' or 'sloped'.");
		labeledTextFieldStyle.field.setColumns(10);
		labeledTextFieldStyle.setMaximumSize(new Dimension(2147483647, 32));
		panelDefFields.add(labeledTextFieldStyle);
		
		labeledTextFieldBg = new JLabeledTextField("Background:");
		labeledTextFieldBg.field.setToolTipText("The stage background. It may be an image path (i.e. '/assets/bg/mybg1.jpg') or a color (i.e. #FFDD55).");
		labeledTextFieldBg.field.setColumns(10);
		labeledTextFieldBg.setMaximumSize(new Dimension(2147483647, 32));
		panelDefFields.add(labeledTextFieldBg);
		
		labeledTextFieldSong = new JLabeledTextField("Song:");
		labeledTextFieldSong.field.setToolTipText("The stage's song. It must be a path to an music file (i.e. '/music/mysong.ogg')");
		labeledTextFieldSong.field.setColumns(10);
		labeledTextFieldSong.setMaximumSize(new Dimension(2147483647, 32));
		panelDefFields.add(labeledTextFieldSong);
		
		panelNoIntroTune = new JPanel();
		panelDefFields.add(panelNoIntroTune);
		
		chckbxNoIntroTune = new JCheckBox("No intro tune");
		chckbxNoIntroTune.setToolTipText("If marked, the game will skip the intro tune and, if present, play the stage's song directly.");
		panelNoIntroTune.add(chckbxNoIntroTune);
		splitPaneLeft.setDividerLocation(128);
		splitPaneMain.setDividerLocation(256);
		splitPaneLeft.setDividerLocation(controller.commons.leftSplitPanePosition);
		splitPaneMain.setDividerLocation(controller.commons.mainSplitPosition);
		
		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		frame.getContentPane().add(toolBar, BorderLayout.SOUTH);
		
		lblMapSize = new JLabel("Map size:");
		toolBar.add(lblMapSize);
		
		spinnerWidth = new JSpinner();
		spinnerWidth.setModel(new SpinnerNumberModel(new Integer(30), new Integer(1), null, new Integer(1)));
		spinnerWidth.setMaximumSize(new Dimension(64, 32767));
		spinnerWidth.addChangeListener(listener);
		toolBar.add(spinnerWidth);
		
		lblX = new JLabel(" x ");
		toolBar.add(lblX);
		
		spinnerHeight = new JSpinner();
		spinnerHeight.setModel(new SpinnerNumberModel(new Integer(30), new Integer(1), null, new Integer(1)));
		spinnerHeight.setMaximumSize(new Dimension(64, 32767));
		spinnerHeight.addChangeListener(listener);
		toolBar.add(spinnerHeight);
		
		horizontalStrut = Box.createHorizontalStrut(24);
		toolBar.add(horizontalStrut);
		
		lblGridSize = new JLabel("Grid size:");
		lblGridSize.setToolTipText("The size of each block. It works as a zoom. Its only meant for visualization.");
		toolBar.add(lblGridSize);
		
		spinnerGridSlotSize = new JSpinner();
		spinnerGridSlotSize.setModel(new SpinnerNumberModel(MainWindow.controller.commons.gridSlotSize, new Integer(8), null, new Integer(1)));
		spinnerGridSlotSize.setMaximumSize(new Dimension(64, 32767));
		spinnerGridSlotSize.addChangeListener(listener);
		toolBar.add(spinnerGridSlotSize);
		
		btnReset = new JButton("Reset");
		btnReset.addActionListener(listener);
		
		horizontalStrut_2 = Box.createHorizontalStrut(8);
		toolBar.add(horizontalStrut_2);
		toolBar.add(btnReset);
		
		horizontalStrut_1 = Box.createHorizontalStrut(24);
		toolBar.add(horizontalStrut_1);
		
		tglbtnBrush = new JToggleButton(new ImageIcon(controller.path + "/pencil.png"));
		if(tglbtnBrush.getIcon() == null) tglbtnBrush.setText("Brush");
		tglbtnBrush.setToolTipText("Brush");
		tglbtnBrush.setSelected(true);
		tglbtnBrush.addActionListener(listener);
		toolBar.add(tglbtnBrush);
		
		tglbtnBucket = new JToggleButton(new ImageIcon(controller.path + "/bucket.png"));
		if(tglbtnBucket.getIcon() == null) tglbtnBucket.setText("Bucket");
		tglbtnBucket.setToolTipText("Bucket fill");
		tglbtnBucket.addActionListener(listener);
		toolBar.add(tglbtnBucket);
		
		this.frame.setSize(800, 600);
		this.frame.setSize(controller.commons.windowWidth, controller.commons.windowHeight);
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		createNewGrid();
	}
	
	public void show()
	{
		this.frame.setVisible(true);
		if(controller.commons.maximized) this.frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}
	
	void showAboutDialog()
	{
		JOptionPane.showMessageDialog(frame, "freepac-editor" + '\n'
				+ "A simple stage editing tool for the game FreePac." + '\n'
				+ "Version "+Main.VERSION + '\n' + '\n'
				+ Main.LICENSE, "About freepac-editor", JOptionPane.INFORMATION_MESSAGE);
	}
	
	char[][] getCharMatrix()
	{
		char[][] grid = new char[gridPanels.getRowCount()][gridPanels.getColumnsCount()];
		
		for(int i = 0; i < gridPanels.getRowCount(); i++) for(int j = 0; j < gridPanels.getColumnsCount(); j++)
			grid[i][j] = controller.gridSlotValueToChar(gridPanels.get(i, j));

		return grid;
	}
	
	void saveStage()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setMultiSelectionEnabled(false);
		if(controller.commons.lastSavePath != null && new File(controller.commons.lastSavePath).isDirectory()) chooser.setCurrentDirectory(new File(controller.commons.lastSavePath));
		
		int status;
		status = chooser.showSaveDialog(frame);
		if(status == JFileChooser.APPROVE_OPTION)
		{
			if(chooser.getSelectedFile().exists() && JOptionPane.NO_OPTION==JOptionPane.showConfirmDialog(frame, "The file already exists. Do you want to overwrite it?", "Overwrite?", JOptionPane.YES_NO_OPTION) )
				return;
			controller.commons.lastSavePath = chooser.getSelectedFile().getParentFile().getAbsolutePath();
			try 
			{
				controller.saveStageFile(getCharMatrix(), getDefinitions(), chooser.getSelectedFile().getAbsolutePath());
				modified = false;
				JOptionPane.showMessageDialog(frame, "Stage " + chooser.getSelectedFile().getName() + " saved successfully.", "Stage saved", JOptionPane.INFORMATION_MESSAGE);
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(frame, e.getLocalizedMessage(), "Could not save stage!", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
	}
	
	void loadStage()
	{
		if(promptDiscard()) return;
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setMultiSelectionEnabled(false);
		if(controller.commons.lastLoadPath != null && new File(controller.commons.lastLoadPath).isDirectory()) chooser.setCurrentDirectory(new File(controller.commons.lastLoadPath));
		chooser.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return ".def (Stage definition file)";
			}
			
			@Override
			public boolean accept(File f) {
				if(f.isDirectory()) return true;
				if( ! f.getName().endsWith(".def")) return false;
				try 
				{
					return new File(f.getCanonicalPath().replace(".def", ".map")).getCanonicalFile().getParentFile().equals(f.getCanonicalFile().getParentFile());
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
					return false;
				} 
			}
		});
		
		int status;
		status = chooser.showOpenDialog(frame);
		if(status == JFileChooser.APPROVE_OPTION) try
		{
			controller.commons.lastLoadPath = chooser.getSelectedFile().getParentFile().getAbsolutePath();
			controller.loadStageFile(chooser.getSelectedFile().getAbsolutePath());
			loadDefinitions(controller.currentStage.definitions);
			loadGrid(controller.currentStage.grid);
			modified = false;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(frame, e.getLocalizedMessage(), "Could not load stage!", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	void createNewGrid()
	{
		if(promptDiscard()) return;
		gridPanels = new Matrix<GridSlot>(30, 30);
		panelGrid.removeAll();
		panelGrid.setMaximumSize(new Dimension(gridPanels.getColumnsCount() * MainWindow.controller.commons.gridSlotSize, gridPanels.getRowCount() * MainWindow.controller.commons.gridSlotSize));
		panelGrid.setLayout(new GridLayout(gridPanels.getRowCount(), gridPanels.getColumnsCount()));
		
		for(int i = 0; i < gridPanels.getRowCount(); i++) for(int j = 0; j < gridPanels.getColumnsCount(); j++)
		{
			GridSlot panel = new GridSlot("nothing");
			panel.addMouseListener(listener);
			gridPanels.set(i, j, panel);
			panelGrid.add(panel);
		}
		
		modified = false;
		panelGrid.updateUI();
	}
	
	void loadGrid(char[][] grid)
	{
		gridPanels = new Matrix<GridSlot>(grid[0].length, grid.length);
		panelGrid.removeAll();
		panelGrid.setLayout(new GridLayout(gridPanels.getRowCount(), gridPanels.getColumnsCount()));
		panelGrid.setMaximumSize(new Dimension(gridPanels.getColumnsCount() * MainWindow.controller.commons.gridSlotSize, gridPanels.getRowCount() * MainWindow.controller.commons.gridSlotSize));
		
		for(int i = 0; i < gridPanels.getRowCount(); i++) for(int j = 0; j < gridPanels.getColumnsCount(); j++)
		{
			GridSlot panel = new GridSlot(controller.charToGridSlotValue(grid[i][j]));
			panel.addMouseListener(listener);
			gridPanels.set(i, j, panel);
			panelGrid.add(panel);
		}
		
		spinnerWidth.setValue(gridPanels.getColumnsCount());
		spinnerHeight.setValue(gridPanels.getRowCount());
		
		panelGrid.updateUI();
	}
	
	void loadDefinitions(Properties definitions)
	{
		String name = definitions.getProperty("name"); if(name != null) name = name.trim();
		String color = definitions.getProperty("color"); if(color != null) color = color.trim();
		String enemies = definitions.getProperty("enemies"); if(enemies != null) enemies = enemies.trim();
		
		if(definitions.getProperty("ghosts") != null && enemies == null)
		{
			JOptionPane.showMessageDialog(frame, "The loaded stage contains deprecated fields. If you save this stage, these fields may be removed.", "Warning", JOptionPane.WARNING_MESSAGE);
			enemies = definitions.getProperty("ghosts"); // for now, allow loading legacy property
		}
		
		if(enemies == null)
			enemies = "default";
		
		//optionals
		String style = definitions.containsKey("style") ? definitions.getProperty("style") : "";
		String bg = definitions.containsKey("background") ? definitions.getProperty("background") : "";
		String song = definitions.containsKey("song") ? definitions.getProperty("song") : "";
		String noIntroTune = definitions.containsKey("nointrotune") ? definitions.getProperty("nointrotune") : "false";
		
		labeledTextFieldName.field.setText(name);
		labeledTextFieldColor.field.setText(color);
		labeledTextFieldEnemies.field.setText(enemies);
		labeledTextFieldBg.field.setText(bg);
		labeledTextFieldSong.field.setText(song);
		labeledTextFieldStyle.field.setText(style);
		chckbxNoIntroTune.setSelected(Boolean.parseBoolean(noIntroTune));
	}
	
	Properties getDefinitions()
	{
		Properties defs = new Properties();
		defs.setProperty("name", labeledTextFieldName.field.getText());
		defs.setProperty("color", labeledTextFieldColor.field.getText());
		defs.setProperty("enemies", labeledTextFieldEnemies.field.getText());
		defs.setProperty("background", labeledTextFieldBg.field.getText());
		defs.setProperty("song", labeledTextFieldSong.field.getText());
		defs.setProperty("style", labeledTextFieldStyle.field.getText());
		defs.setProperty("nointrotune", Boolean.toString(chckbxNoIntroTune.isSelected()));
		return defs;
	}
	
	String getTintValue()
	{
		if(rdbtnClear.isSelected())
			return ("nothing");
		
		if(rdbtnWall.isSelected())
			return ("wall");
		
		if(rdbtnFence.isSelected())
			return ("fence");
		
		if(rdbtnOutside.isSelected())
			return ("outside");
		
		if(rdbtnHorizWarp.isSelected())
			return ("h warp");
		
		if(rdbtnVerticWarp.isSelected())
			return ("v warp");
		
		if(rdbtnSlowZone.isSelected())
			return ("slow zone");
		
		if(rdbtnPlayerStart.isSelected())
			return ("player start");

		if(rdbtnEnemyStart.isSelected())
			return ("enemy start");
		
		if(rdbtnPellet.isSelected())
			return ("pellet");
		
		if(rdbtnPelletAndSlow.isSelected())
			return ("pellet and slow zone");
		
		if(rdbtnEnergizer.isSelected())
			return ("energizer");
		
		return "nothing";
	}
	
	void mark(GridSlot p)
	{
		p.mark(getTintValue());
		modified = true;
		panelGrid.updateUI();
	}
	
	void bucketFill(GridSlot p, String target)
	{
		if( target.equals(getTintValue())) return; //avoid dumbness
		if(!p.value.equals(target)) return; //not our target, ignore
		
		mark(p);
		if(gridPanels.indexesOf(p) != null)
		{
			int[] indexes = gridPanels.indexesOf(p);
			
			if(indexes[0] > 0) 								bucketFill(gridPanels.get(indexes[0]-1, indexes[1]), target);
			if(indexes[1] > 0) 								bucketFill(gridPanels.get(indexes[0], indexes[1]-1), target);
			if(indexes[0] < gridPanels.getRowCount()-1) 	bucketFill(gridPanels.get(indexes[0]+1, indexes[1]), target);
			if(indexes[1] < gridPanels.getColumnsCount()-1) bucketFill(gridPanels.get(indexes[0], indexes[1]+1), target);
		}
	}
	
	void updateGridSize()
	{
		try {
			while(gridPanels.getColumnsCount() < (Integer) spinnerWidth.getValue())
				gridPanels.appendColumn(GridSlot.class);
			
			while(gridPanels.getRowCount() < (Integer) spinnerHeight.getValue())
				gridPanels.appendRow(GridSlot.class);
			
			while(gridPanels.getColumnsCount() > (Integer) spinnerWidth.getValue())
				gridPanels.removeLastColumn();
			
			while(gridPanels.getRowCount() > (Integer) spinnerHeight.getValue())
				gridPanels.removeLastRow();
		
		} catch (Exception e) { e.printStackTrace(); JOptionPane.showMessageDialog(frame, e.getLocalizedMessage(), "Critical error!", JOptionPane.ERROR_MESSAGE); return;}
		
		panelGrid.removeAll();
		panelGrid.setLayout(new GridLayout(gridPanels.getRowCount(), gridPanels.getColumnsCount()));
		panelGrid.setMaximumSize(new Dimension(gridPanels.getColumnsCount() * MainWindow.controller.commons.gridSlotSize, gridPanels.getRowCount() * MainWindow.controller.commons.gridSlotSize));
		
		for(GridSlot slot : gridPanels)
		{
			slot.addMouseListener(listener);
			panelGrid.add(slot);
		}
		
		panelGrid.updateUI();
	}
	
	boolean promptDiscard()
	{
		return modified && JOptionPane.NO_OPTION==JOptionPane.showConfirmDialog(frame, "There are unsaved changes on the current stage. Do you want to discard them?", "Discard changes?", JOptionPane.YES_NO_OPTION);
	}
	
	void promptExit()
	{
		if(promptDiscard()) return;
		controller.commons.leftSplitPanePosition = splitPaneLeft.getDividerLocation();
		controller.commons.mainSplitPosition = splitPaneMain.getDividerLocation();
		controller.commons.maximized = (frame.getExtendedState() & JFrame.MAXIMIZED_BOTH) == JFrame.MAXIMIZED_BOTH;
		if(!controller.commons.maximized)
		{
			controller.commons.windowWidth = frame.getWidth();
			controller.commons.windowHeight = frame.getHeight();
		}
		controller.shutdown();
	}
	
	class EventListener implements ActionListener, MouseListener, WindowListener, ChangeListener
	{
		boolean isMousePressed = false;
		@Override
		public void actionPerformed(ActionEvent e) {
			Object src = e.getSource();
			
			if(src == mntmNewStage)
				createNewGrid();
			
			if(src == mntmAbout)
				showAboutDialog();
			
			if(src == mntmExit) 
				promptExit();
			
			if(src == mntmSaveStage)
				saveStage();
			
			if(src == mntmLoadStage)
				loadStage();
			
			if(src == btnReset)
				spinnerGridSlotSize.setValue(32);
			
			if(src == tglbtnBrush)
				tglbtnBucket.setSelected(false);
			
			if(src == tglbtnBucket)
				tglbtnBrush.setSelected(false);
		}

		@Override
		public void mouseClicked(MouseEvent e) { }

		@Override
		public void mousePressed(MouseEvent e) 
		{ 
			Object src = e.getSource();
			
			if(tglbtnBrush.isSelected() && src instanceof GridSlot && gridPanels.contains((GridSlot) src))
			{
				mark((GridSlot) src);
				isMousePressed = true;
			}
			
			if(tglbtnBucket.isSelected() && src instanceof GridSlot && gridPanels.contains((GridSlot) src))
			{
				bucketFill((GridSlot) src, ((GridSlot) src).value);
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) 
		{ 
			isMousePressed = false;
		}

		@Override
		public void mouseEntered(MouseEvent e) 
		{
			Object src = e.getSource();
			
			if(tglbtnBrush.isSelected() && isMousePressed && src instanceof GridSlot && gridPanels.contains((GridSlot) src))
				mark((GridSlot) src);
		}

		@Override
		public void mouseExited(MouseEvent e) { }

		@Override
		public void windowOpened(WindowEvent e) { }

		@Override
		public void windowClosing(WindowEvent e) 
		{ 
			promptExit();
		}

		@Override
		public void windowClosed(WindowEvent e) { }

		@Override
		public void windowIconified(WindowEvent e) { }

		@Override
		public void windowDeiconified(WindowEvent e) { }

		@Override
		public void windowActivated(WindowEvent e) { }

		@Override
		public void windowDeactivated(WindowEvent e) { }

		@Override
		public void stateChanged(ChangeEvent e)
		{
			Object src = e.getSource();
			
			if(src == spinnerGridSlotSize)
			{
				MainWindow.controller.commons.gridSlotSize = (Integer) spinnerGridSlotSize.getValue();
				updateGridSize();
			}
			
			if(src == spinnerWidth || src == spinnerHeight)
				updateGridSize();
		}
		
	} 
}
